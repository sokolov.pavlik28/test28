<?php

use App\Models\CarBrand;
use App\Models\CarModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(CarBrand::class)->constrained()->cascadeOnUpdate()->cascadeOnDelete();
            $table->foreignIdFor(CarModel::class)->constrained()->cascadeOnUpdate()->cascadeOnDelete();
            $table->unsignedSmallInteger('year')->nullable()->digits(4);
            $table->unsignedBigInteger('mileage')->nullable();
            $table->string('color', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cars');
    }
};
