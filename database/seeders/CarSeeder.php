<?php

namespace Database\Seeders;

use App\Models\Car;
use App\Models\CarBrand;
use App\Models\CarModel;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $carModels = CarModel::select('id', 'car_brand_id')->get();

        foreach ($carModels as $car) {
            Car::create([
                'car_brand_id' => $car->car_brand_id,
                'car_model_id' => $car->id,
                'year' => fake()->numberBetween(2000, 2024),
                'mileage' => fake()->numberBetween(0, 100000),
                'color' => fake()->colorName,
            ]);
        }
    }
}
