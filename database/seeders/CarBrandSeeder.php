<?php

namespace Database\Seeders;

use App\Models\CarBrand;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CarBrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $brands = [
            'Toyota',
            'Honda',
            'Ford',
            'Chevrolet',
            'Volkswagen',
            'Nissan',
            'BMW',
            'Mercedes-Benz',
            'Audi',
            'Subaru'
        ];

        foreach ($brands as $brand) {
            CarBrand::create(
              ['title' => $brand]
            );
        }
    }
}
