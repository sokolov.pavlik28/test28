<?php

namespace Database\Seeders;

use App\Models\CarBrand;
use App\Models\CarModel;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CarModelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $model = [
            'Toyota' => 'Camry',
            'Honda' => 'Civic',
            'Ford' => 'Mustang',
            'Chevrolet' => 'Corvette',
            'Volkswagen' => 'Golf',
            'Nissan' => 'Altima',
            'BMW' => '3 Series',
            'Mercedes-Benz' => 'E-Class',
            'Audi' => 'A4',
            'Subaru' => 'Outback'
        ];

        foreach ($model as $model => $modelTitle) {
            $carBrand = CarBrand::where('title', $model)->first();
            if($carBrand) {
                CarModel::create([
                    'car_brand_id' => $carBrand->id,
                    'title' => $modelTitle
                ]);
            }else {
                continue;
            }
        }
    }
}
