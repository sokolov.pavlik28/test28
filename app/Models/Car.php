<?php

namespace App\Models;

use App\Http\Resources\CarResource;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Car extends Model
{
    use HasFactory;

    protected $fillable = [
        'car_brand_id',
        'car_model_id',
        'year',
        'mileage',
        'color'
    ];

    public static function checkCar(int $id)
    {
        try {
            return new CarResource(self::findOrFail($id));
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'message' => "Машина не найдена с ID - $id",
            ], 404);
        }
    }

    public static function createBrand(string $title)
    {
        return CarBrand::create([
            'title' => $title
        ]);
    }

    public static function createModel(string $title, int $idBrand)
    {
        return CarModel::create([
            'title' => $title,
            'car_brand_id' => $idBrand
        ]);
    }

    public static function showCar(int $id)
    {
        return self::checkCar($id);
    }

    public function brand(): BelongsTo
    {
        return $this->belongsTo(CarBrand::class, 'car_brand_id');
    }

    public function model(): BelongsTo
    {
        return $this->belongsTo(CarModel::class, 'car_model_id');
    }
}
