<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\CarStoreRequest;
use App\Http\Resources\CarResource;
use App\Models\Car;
use App\Models\CarBrand;
use App\Models\CarModel;
use Illuminate\Http\Response;

class CarController extends Controller
{
    public function index()
    {
        return CarResource::collection(Car::with(['model', 'brand'])->get());
    }

    public function store(CarStoreRequest $request, Car $car)
    {
        $data = $request->only(['model', 'brand', 'year', 'mileage', 'color']);

        $carBrand = Car::createBrand($data['brand']);
        $carModel = Car::createModel($data['model'], $carBrand->id);

        $car = Car::create([
            'car_brand_id' => $carBrand->id,
            'car_model_id' => $carModel->id,
            'year' => $data['year'] ?? null,
            'mileage' => $data['mileage'] ?? null,
            'color' => $data['color'] ?? null,
        ]);

        return new CarResource($car);
    }

    public function show(int $id)
    {
        return Car::showCar($id);
    }

    public function update(CarStoreRequest $request, int $id)
    {
        $car = Car::find($id);
        if (!$car) {
            return response()->json(['message' => "Машина не найдена с ID - $id"], 404);
        }

        $data = $request->only(['model', 'brand', 'year', 'mileage', 'color']);

        foreach (['brand', 'model'] as $key) {
            if (!array_key_exists($key, $data)) {
                $data[$key] = $car->$key->title;
            }
        }
        CarBrand::where('id', $car->car_brand_id)->update(['title' => $data['brand']]);
        CarModel::where('id', $car->car_model_id)->update(['title' => $data['model']]);

        $carBrandId = CarBrand::where('title', $data['brand'])->first()->id;
        $carModelId = CarModel::where('title', $data['model'])->first()->id;

        Car::find($car->id)->update([
            'car_brand_id' => $carBrandId ?? $car->car_brand_id,
            'car_model_id' => $carModelId ?? $car->car_brand_id,
            'year' => $request->has('year') ? $data['year'] : $car->year,
            'mileage' => $request->has('mileage') ? $data['mileage'] : $car->mileage,
            'color' => $request->has('color') ? $data['color'] : $car->color
        ]);

        return new CarResource(Car::find($id));
    }

    public function destroy(int $id)
    {
        $car = Car::find($id);

        if (!$car) {
            return response()->json([
                'message' => "Машина не найдена с ID - $id",
            ], 404);
        }

        $car->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
