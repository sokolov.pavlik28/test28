<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\CarModelResource;
use App\Models\CarModel;


class CarModelController extends Controller
{
    public function index()
    {
        return CarModelResource::collection(CarModel::with('brand')->get());
    }
}
