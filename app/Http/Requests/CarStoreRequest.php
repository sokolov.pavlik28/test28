<?php

namespace App\Http\Requests;

use App\Models\Car;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Client\Response;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Foundation\Http\FormRequest;


class CarStoreRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(Request $request): array
    {
        $rules = [
            'brand' => ['regex:/^[A-Za-zА-Яа-яЁё\s-]+$/u'],
            'model' => ['regex:/^[0-9A-Za-zА-Яа-яЁё\s-]+$/u'],
            'year' => ['nullable', 'integer', 'regex:/^[0-9]{4}$/u'],
            'mileage' => ['nullable', 'integer'],
            'color' => ['nullable', 'regex:/^[A-Za-z]+$/u']
        ];

        if ($request->isMethod('post') || $request->isMethod('put')) {
            $rules['brand'][] = 'required';
            $rules['model'][] = 'required';
        }

        if ($request->isMethod('patch')) {
            $rules['brand'][] = 'sometimes';
            $rules['brand'][] = 'filled';
            $rules['model'][] = 'sometimes';
            $rules['model'][] = 'filled';
        }
        return $rules;
    }

    protected function failedValidation(Validator $validator)
    {
        $response = new JsonResponse([
            'message' => 'Ошибка валидации',
            'errors' => $validator->errors()
        ], 422);

        throw (new ValidationException($validator, $response))
            ->errorBag($this->errorBag)
            ->redirectTo($this->getRedirectUrl());
    }

    public function messages(): array
    {
        return [
            'brand.required' => 'Марка автомобиля обязательна для заполнения',
            'brand.regex' => 'Разрешены символы кириллицы, латиницы, пробелы и тире',
            'brand.filled' => 'Марка автомобиля не должна быть пустой',
            'model.required' => 'Модель автомобиля обязательная для заполнения',
            'model.regex' => 'Разрешены символы кириллицы, латиницы, цифры, пробелы и тире',
            'model.filled' => 'Модель автомобиля не должна быть пустой',
            'year.integer' => 'Год выпуска должен быть числом',
            'year.regex' => 'Год выпуска должен состоять из 4 цифр',
            'mileage.integer' => 'Пробег машины должен быть числом',
            'color.regex' => 'Разрешены символы латиницы'
        ];
    }
}
