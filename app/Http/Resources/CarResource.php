<?php

namespace App\Http\Resources;

use App\Models\Car;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CarResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'brand' => $this->brand->title,
            'model' => $this->model->title,
            'year' => $this->year,
            'mileage' => $this->mileage,
            'color' => $this->color,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
